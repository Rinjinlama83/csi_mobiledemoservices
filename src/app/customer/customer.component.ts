import { Component, OnInit } from "@angular/core";
import { CustomerService } from "../customer.service";
import { Router } from "@angular/router";
import { HelperService } from "../helper.service";

@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.css"]
})
export class CustomerComponent implements OnInit {
  searchText: string;
  public customers: any = [];
  toggleMenu: boolean = false;
  constructor(
    private customerService: CustomerService,
    private router: Router,
    private helper: HelperService
  ) {}

  ngOnInit() {
    this.getCustomer();
    this.detectBackButton();
  }
  detectBackButton() {
    this.helper.ShowNavMenuButton("Customers");
  }

  getCustomer() {
    this.customerService.getCustomers().subscribe(data => {
      this.customers = data.Items;
    });
  }
  onClickCustomer(Id) {
    this.router.navigate([`/customers/customerorders/${Id}`]);
  }
  onMenuClick() {
    this.toggleMenu ? (this.toggleMenu = false) : (this.toggleMenu = true);
  }

  getTempCustomer() {
    this.customers = [
      [
        {
          Name: "CustNum",
          Value: "      1"
        },
        {
          Name: "Name",
          Value: "Coordinated Bicycles"
        },
        {
          Name: "Addr_1",
          Value: "57460 Dewitt St"
        },
        {
          Name: "CustSeq",
          Value: "0"
        },
        {
          Name: "_ItemId",
          Value:
            "PBT=[customer] cus.ID=[d6b8d3fd-849b-4af0-b179-d3d0260c379e] cus.DT=[2018-09-05 10:04:42.007] adr.DT=[2018-06-25 15:06:19.423] adr.ID=[a0032871-3cab-438d-84df-c65c7263c374]"
        }
      ],
      [
        {
          Name: "CustNum",
          Value: "      2"
        },
        {
          Name: "Name",
          Value: "Price Brothers Dept Store"
        },
        {
          Name: "Addr_1",
          Value: "44 E Main St"
        },
        {
          Name: "CustSeq",
          Value: "0"
        },
        {
          Name: "_ItemId",
          Value:
            "PBT=[customer] cus.ID=[83f2f669-e2eb-42cc-8615-59c368409e62] cus.DT=[2018-08-02 14:12:02.173] adr.DT=[2018-05-08 11:18:43.010] adr.ID=[ce3ed78d-df8f-4ee8-82e1-ba219b04e49b]"
        }
      ]
    ];
  }
}
