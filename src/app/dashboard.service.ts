import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Rx";

@Injectable()
export class DashboardService {
  constructor(private http: HttpClient) {}

  getPipeLine(): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
        "X-Infor-MongooseConfig": localStorage.getItem("config")
      })
    };
    return this.http.get(
      `https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/json/SLExecutives/DerPipelineValue,Stage/adv/?customloadmethod=CLM_ExecutivePipelineSp&customloadmethodparms=G, , , ,&/`,
      httpOptions
    );
  }

  getCashImpact(): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
        "X-Infor-MongooseConfig": localStorage.getItem("config")
      })
    };
    return this.http.get(
     `https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/json/SLControllers/GraphAmount,GraphDate/adv?customloadmethod=Home_MetricCashImpactSp&e/`,
      httpOptions
    );
  }
}
