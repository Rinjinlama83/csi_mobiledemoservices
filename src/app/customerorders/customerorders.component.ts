import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CustomerService } from "../customer.service";
import { HelperService } from "../helper.service";

@Component({
  selector: "app-customerorders",
  templateUrl: "./customerorders.component.html",
  styleUrls: ["./customerorders.component.css"]
})
export class CustomerordersComponent implements OnInit {
  custOrder: Array<any> = [];
  constructor(
    private activateroute: ActivatedRoute,
    private customerService: CustomerService,
    private helper: HelperService
  ) {}

  ngOnInit() {
    this.routeParam();
  }

  routeParam() {
    this.activateroute.params.subscribe(data => {
      this.detectBackButton(data.id);
      this.getCustomerOrder(data.id);
    });
  }

  detectBackButton(id) {
    //show back button
    this.helper.ShowNavMenuButton("Customer - " + id);
  }

  getCustomerOrder(id: string) {
    this.customerService.getCustomerOrders(id).subscribe(
      data => {
        this.custOrder = data.Items;
      },
      err => {
        alert(err);
      }
    );
  }
}
