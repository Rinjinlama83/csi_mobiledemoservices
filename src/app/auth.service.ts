import { Injectable } from "@angular/core";
import { Component } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {
 
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem("token");
    //Mongoose user Auth1.0a, so it's never expired, but check it token is valid or not by sending a request
    return token != null ? true : false;
  }

  public getUserName(): string {
    return localStorage.getItem("user") || "Infor";
  }

  public Authenticated(config, tempheaders) {
    if (location.hostname === "localhost") {
      return Observable.from("test");
    } else {
      const headers = new HttpHeaders()
        .set("Content-Type", "application/html")
        .set("UserId", tempheaders.UserId)
        .set("Password", tempheaders.Password);
      return this.http.get(
        `https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/js/token/${config}`,
        {
          headers: headers,
          responseType: "text"
        }
      );
    }
  }

  getConfiguration(): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/html"}) };
      return this.http.get(
      `https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/json/configurations`,
      httpOptions
    );
  }
}
