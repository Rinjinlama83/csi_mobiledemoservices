import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCust'
})
export class FilterCustPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
      searchText = searchText.toLowerCase();
      return items.filter((it,index,array)=>{
        return it[1].Value.toLowerCase().includes(searchText);
      });
   }

}
