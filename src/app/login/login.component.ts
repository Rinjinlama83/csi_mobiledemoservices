import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  config: string;
  configuration: string[];
  constructor(private authservice: AuthService, private router: Router) {}

  ngOnInit() {
    //this.configuration = ["test", "test"];
      this.getConfiguration();
  }
  LogIn() {
    let tempheaders = { UserId: this.username, Password: this.password };
    this.authservice.Authenticated(this.config, tempheaders).subscribe(
      data => {
        localStorage.setItem("token", data.toString());
        localStorage.setItem("user", this.username);
        localStorage.setItem("config", this.config);
        this.router.navigate(["/home"]);
      },
      err => console.log(JSON.stringify(err))
    );
  }

  getConfiguration() {
    this.authservice.getConfiguration().subscribe(data => {
      this.configuration = data;
    });
  }
}
