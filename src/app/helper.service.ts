import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class HelperService {
  private _subject: Subject<any> = new Subject();
  private _subjectNavList: Subject<any> = new Subject();
  constructor() {}

  ShowNavMenuButton(headerTitle: string) {
    this._subject.next(headerTitle);
  }

  HideNavMenuButton(): Observable<any> {
    return this._subject.asObservable();
  }

  ShowNavMenuList(headerTitle: string) {
    this._subjectNavList.next(headerTitle);
  }

  RegisterNavMenuList(): Observable<any> {
    return this._subject.asObservable();
  }
}
