import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class PurchaseService {
  constructor(private http: HttpClient) {}

  getPurchaseOrder(): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json",
      "Authorization":localStorage.getItem("token"),
      "X-Infor-MongooseConfig":localStorage.getItem("config") }) };
    return this.http.get("https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/json/SLPos/PoNum,VendNum,VendorName,FormatedVendAddress,VendContact,VendPhone,VendFaxNum/adv/", httpOptions);
  }
}
