import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpHeaderResponse
} from "@angular/common/http";
import { Observable } from "rxjs/Rx";

@Injectable()
export class CustomerService {
  constructor(private http: HttpClient) {}

  getCustomers(): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json",
      "Authorization":localStorage.getItem("token"),
      "X-Infor-MongooseConfig":localStorage.getItem("config") }) };
    return this.http.get(
      `https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/json/Slcustomers/CustNum,Name,Addr_1,CustSeq/adv/?filter=CustSeq=0&/e/`,
      // "http://localhost:3000/customers",
      httpOptions
    );
  }
  getCustomerOrders(Id): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json",
      "Authorization":localStorage.getItem("token"),
      "X-Infor-MongooseConfig":localStorage.getItem("config") }) };
    return this.http.get(
      `https://csgde90100be.inforbc.com/IDORequestService/MGRestService.svc/json/SLCos/CustNum,CoNum,Price,Stat,OrderDate,Contact,BillToContact_3,ShipToContact_2,Type/adv/?filter=CustNum='${Id}' and  Type <> 'E'&test/`,
      httpOptions
    );
  }
}
