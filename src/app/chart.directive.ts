import { Directive, ElementRef, Input, Renderer2 } from "@angular/core";

declare var $: any;

@Directive({
  selector: "[appChart]"
})
export class ChartDirective {
  @Input()
  appChart: string;
  @Input()
  charttype: string;
  @Input()
  dataset: any;
  constructor(private elem: ElementRef, private renderer: Renderer2) {}
  ngOnChanges() {
    $(this.elem.nativeElement).chart({
      type: this.charttype,
      dataset: this.dataset
    });
  }
}
