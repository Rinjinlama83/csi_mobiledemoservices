import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { HelperService } from "../helper.service";
import { Location } from "@angular/common";
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-appmenu",
  templateUrl: "./appmenu.component.html",
  styleUrls: ["./appmenu.component.css"]
})
export class AppmenuComponent implements OnInit {
  toggleMenu: boolean = false;
  title: string = "CSI Mobile Demo";
  showBackButton: boolean = false;
  isAuthenticated: boolean;
  user: string;
  constructor(
    private helper: HelperService,
    private location: Location,
    private cdRef: ChangeDetectorRef,
    private authservice: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.onRegisterAppMenu();
  }

  onRegisterAppMenu() {
    this.helper.RegisterNavMenuList().subscribe(data => {
      this.onBackButton();
      this.isAuthenticated = this.authservice.isAuthenticated();
      this.user = this.authservice.getUserName();
      this.cdRef.detectChanges();
    });
  }

  onBackButton() {
    this.helper.HideNavMenuButton().subscribe(data => {
      this.title = data;
      if (
        this.title.toLowerCase() === "customers" ||
        this.title.toLowerCase() === "home" ||
        this.title.toLowerCase() === "purchase orders"
      ) {
        this.showBackButton = false;
      } else {
        this.showBackButton = true;
      }
      this.cdRef.detectChanges();
    });
  }

  onMenuClick() {
    this.toggleMenu ? (this.toggleMenu = false) : (this.toggleMenu = true);
  }
  onBackClick() {
    this.location.back();
  }
  logOut() {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    localStorage.removeItem("config");
    this.router.navigateByUrl("/");
    location.reload();
  }
}
