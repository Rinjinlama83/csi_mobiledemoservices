import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { AppmenuComponent } from "./appmenu/appmenu.component";
import { CustomerComponent } from "./customer/customer.component";

import { CustomerService } from "./customer.service";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule} from '@angular/http';
import { PurchaseorderComponent } from "./purchaseorder/purchaseorder.component";
import { CustomerordersComponent } from "./customerorders/customerorders.component";
import { HomeComponent } from "./home/home.component";
import { CustomerOutletComponent } from "./customer-outlet/customer-outlet.component";
import { HelperService } from "./helper.service";
import { PurchaseService } from "./purchase.service";
import { LoginComponent } from "./login/login.component";
import { AuthService } from "./auth.service";
import { AuthGuard } from "./auth.guard";
import { AboutComponent } from "./about/about.component";
import { FilterCustPipe } from "./filter-cust.pipe";
import { DashboardService } from "./dashboard.service";
import { ChartDirective } from "./chart.directive";

const appRoutes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "home", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "about", component: AboutComponent },
  {
    path: "customers",
    component: CustomerOutletComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        component: CustomerComponent
      },
      {
        path: "customerorders/:id",
        component: CustomerordersComponent
      }
    ]
  },
  {
    path: "purchaseorder",
    component: PurchaseorderComponent,
    canActivate: [AuthGuard]
  },
  { path: "login", component: LoginComponent },
  { path: "**", component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AppmenuComponent,
    CustomerComponent,
    PurchaseorderComponent,
    CustomerordersComponent,
    HomeComponent,
    CustomerOutletComponent,
    LoginComponent,
    AboutComponent,
    FilterCustPipe,
    ChartDirective
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false, useHash: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    CustomerService,
    AuthGuard,
    HelperService,
    PurchaseService,
    AuthService,
    DashboardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
