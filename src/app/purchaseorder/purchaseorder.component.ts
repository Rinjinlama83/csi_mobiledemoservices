import { Component, OnInit } from "@angular/core";
import { PurchaseService } from "../purchase.service";
import { Router } from "@angular/router";
import { HelperService } from "../helper.service";

@Component({
  selector: "app-purchaseorder",
  templateUrl: "./purchaseorder.component.html",
  styleUrls: ["./purchaseorder.component.css"]
})
export class PurchaseorderComponent implements OnInit {
  purchaseorder: any = [];
  toggleMenu: boolean = false;
  constructor(
    private purchaseorderservice: PurchaseService,
    private router: Router,
    private helper: HelperService
  ) {}

  ngOnInit() {
    this.getPurchaseOrders();
    //menu
    this.helper.ShowNavMenuButton("Purchase Orders");
  }
  getPurchaseOrders() {
    this.purchaseorderservice.getPurchaseOrder().subscribe(data => {
      this.purchaseorder = data.Items;
      console.log(this.purchaseorder);
    });
  }
  onClickCustomer(Id) {
    this.router.navigate([`/purchaseorder/customerorders/${Id}`]);
    //show back button
    this.helper.ShowNavMenuButton("Customer - " + Id);
  }
  onMenuClick() {
    this.toggleMenu ? (this.toggleMenu = false) : (this.toggleMenu = true);
  }
}
