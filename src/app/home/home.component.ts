import { Component, OnInit } from "@angular/core";
import { HelperService } from "../helper.service";
import { DashboardService } from "../dashboard.service";

declare var $: any;

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  dataset: any = [];
  charttype: any;
  cashImpactdataset: any = [];
  cashImpactcharttype: any = [];
  constructor(
    private helper: HelperService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit() {
    //menu
    this.helper.ShowNavMenuButton("Home");
    this.getPipeLine();
   // this.getCashImpact();
    this.temCashImpact();
    this.flipanimation();
  }

  getCashImpact() {
    this.dashboardService.getPipeLine().subscribe(res => {
      let data = [];
      res.Items.forEach(element => {
        element[1].Value == null
          ? ""
          : data.push({
              name: element[1].Value,
              value: element[0].Value
            });
      });
      this.cashImpactdataset = [{ data: data, name: "" }];

      this.cashImpactcharttype = "column";
    });
  }

  getPipeLine() {
    this.dashboardService.getPipeLine().subscribe(res => {
      let data = [];
      res.Items.forEach(element => {
        element[1].Value == null
          ? ""
          : data.push({
              name: element[1].Value,
              value: element[0].Value
            });
      });

      this.dataset = [{ data: data, name: "" }];

      this.charttype = "bar";
    });
  }

  flipanimation(){
    let imgone = <HTMLElement>document.querySelector(".img-one");
    let twoone = <HTMLElement>document.querySelector(".img-two");
    imgone.style.display = "inline";
    imgone.classList.add("flip-image");
    var node = document.createElement("span"); 
    node.id="item-name";     
    node.style.padding="10px";    
    var textnode = document.createTextNode("DSC8902re");         
    node.appendChild(textnode);                              
    imgone.parentNode.appendChild(node); 

    setInterval(() => {
        imgone.classList.remove("flip-image");
        imgone.style.display = "none";
        twoone.style.display = "inline";
        twoone.classList.add("flip-image");
        document.getElementById("item-name").innerHTML="transparent-steel"
    }, 2000);

    setInterval(() => {
        twoone.classList.remove("flip-image");
        twoone.style.display = "none";
        imgone.style.display = "inline";
        imgone.classList.add("flip-image");
        document.getElementById("item-name").innerHTML="DSC8902re"
    }, 4000);
  }

  tempPipeLine() {
    var dataPipe = {
      Items: [
        [
          {
            Name: "DerPipelineValue",
            Value: "0.00000000"
          },
          {
            Name: "Stage",
            Value: null
          },
          {
            Name: "_ItemId",
            Value: null
          }
        ],
        [
          {
            Name: "DerPipelineValue",
            Value: "1000.00000000"
          },
          {
            Name: "Stage",
            Value: "1 - Incoming"
          },
          {
            Name: "_ItemId",
            Value: null
          }
        ],
        [
          {
            Name: "DerPipelineValue",
            Value: "140552.42000000"
          },
          {
            Name: "Stage",
            Value: "2 - Qualify"
          },
          {
            Name: "_ItemId",
            Value: null
          }
        ],
        [
          {
            Name: "DerPipelineValue",
            Value: "1500.00000000"
          },
          {
            Name: "Stage",
            Value: "3 - Present"
          },
          {
            Name: "_ItemId",
            Value: null
          }
        ],
        [
          {
            Name: "DerPipelineValue",
            Value: "14670.00000000"
          },
          {
            Name: "Stage",
            Value: "4 - Propose"
          },
          {
            Name: "_ItemId",
            Value: null
          }
        ],
        [
          {
            Name: "DerPipelineValue",
            Value: "107850.00000000"
          },
          {
            Name: "Stage",
            Value: "6 - Close"
          },
          {
            Name: "_ItemId",
            Value: null
          }
        ]
      ],
      Message: "Success",
      MessageCode: 0
    };

    var data = [];

    dataPipe.Items.forEach(element => {
      element[1].Value == null
        ? ""
        : data.push({
            name: element[1].Value,
            value: element[0].Value
          });
    });

    this.dataset = [{ data: data, name: "" }];

    this.charttype = "bar";
  }

  temCashImpact() {
    let cashImpactCSI = 
    {
      "Items": [
          [
              {
                  "Name": "GraphAmount",
                  "Value": "161.38400000"
              },
              {
                  "Name": "GraphDate",
                  "Value": "09/05/2018"
              },
              {
                  "Name": "_ItemId",
                  "Value": null
              }
          ],
          [
              {
                  "Name": "GraphAmount",
                  "Value": "1686.76400000"
              },
              {
                  "Name": "GraphDate",
                  "Value": "10/05/2018"
              },
              {
                  "Name": "_ItemId",
                  "Value": null
              }
          ],
          [
              {
                  "Name": "GraphAmount",
                  "Value": "1686.76400000"
              },
              {
                  "Name": "GraphDate",
                  "Value": "11/04/2018"
              },
              {
                  "Name": "_ItemId",
                  "Value": null
              }
          ],
          [
              {
                  "Name": "GraphAmount",
                  "Value": "1686.76400000"
              },
              {
                  "Name": "GraphDate",
                  "Value": "12/04/2018"
              },
              {
                  "Name": "_ItemId",
                  "Value": null
              }
          ],
          [
              {
                  "Name": "GraphAmount",
                  "Value": "1686.76400000"
              },
              {
                  "Name": "GraphDate",
                  "Value": "01/03/2019"
              },
              {
                  "Name": "_ItemId",
                  "Value": null
              }
          ]
      ],
      "Message": "Success",
      "MessageCode": 0
  };

    let data = [];

    cashImpactCSI.Items.forEach(element => {
      element[1].Value == null
        ? ""
        : data.push({
            name: element[1].Value,
            value: element[0].Value
          });
    });

    this.cashImpactdataset = [{ data: data, name: "" }];

    this.cashImpactcharttype = "column";
  }
}
